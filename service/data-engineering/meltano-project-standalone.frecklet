doc:
  short_help: Install a Meltano project from git.

args:
  project_name:
    doc:
      short_help: The name of the project.
    type: string
    required: true
    default: meltano
  user:
    doc:
      short_help: The user to install meltano for.
    type: string
    required: true
    default: meltano
  group:
    doc:
      short_help: The group to install meltano for.
    type: string
    required: true
    default: meltano
  path:
    doc:
      short_help: The path to install the Meltano project into.
    type: string
    required: true
    default: /var/lib/meltano
  repo:
    doc:
      short_help: The git repo url of the project.
    type: string
    required: true
  version:
    doc:
      short_help: "The git repo version/branch."
    type: string
    required: false
    default: master
frecklets:
  - git-repo-synced:
      dest: "{{:: path ::}}"
      repo: "{{:: repo ::}}"
      version: "{{:: version ::}}"
      owner: "{{:: user ::}}"
      group: "{{:: group ::}}"
  - python-virtualenv:
      venv_name: "{{:: project_name ::}}"
      user: "{{:: user ::}}"
      python_type: pyenv
      python_version: 3.7.3
      python_packages:
        - meltano
        - jupyter
        - numpy
        - pandas
        - psycopg2
        - sqlalchemy
        - matplotlib
        - gunicorn
        - gevent
#  - python-virtualenv-execute-shell:
#      virtualenv_path: "/home/{{:: user ::}}/.pyenv/versions/{{:: project_name ::}}"
#      user: "{{:: user ::}}"
#      command: "meltano add loader target-postgres"
#      chdir: "{{:: path ::}}"
#  - python-virtualenv-execute-shell:
#      virtualenv_path: "/home/{{:: user ::}}/.pyenv/versions/{{:: project_name ::}}"
#      user: "{{:: user ::}}"
#      command: "meltano add orchestrator airflow"
#      chdir: "{{:: path ::}}"
  - pyenv-gunicorn-systemd-service-unit-file:
      app_module: "meltano.api.wsgi:app"
      venv_name: "{{:: project_name ::}}"
      user: "{{:: user ::}}"
      working_directory: "{{:: path ::}}"
      gunicorn_config: "python:meltano.api.wsgi.config"
      port: 5000
      started: true
      enabled: true
