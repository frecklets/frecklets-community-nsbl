doc:
  short_help: Create a server with the specified name/id exists in your Hetzner hcloud account.
  help: |
    Create a server with the specified name/id exists in your Hetzner hcloud account, if it doesn't exist yet.

    If a server exists, nothing is done, even if the 'image' and 'server_type' are different to the deployed server.
args:
  api_token:
    doc:
      short_help: The hcloud API token.
    type: string
    required: true
    secret: true
  server_name:
    doc:
      short_help: The name of the cloud server to manage (for now, don't use '_', '-', or other special characters in the server name -- will be fixed later).
    type: string
    required: true
  register_target:
    doc:
      short_help: The name of the variable to register the result of this frecklet.
    type: string
    required: false
    default: _register_hcloud_server
    cli:
      metavar: VAR_NAME
  server_type:
    doc:
      short_help: The type of server to create.
    type: string
    allowed:
      - cx11
      - cx21
      - cx31
      - cx41
      - cx51
      - cx11-ceph
      - cx21-ceph
      - cx31-ceph
      - cx41-ceph
      - cx51-ceph
      - ccx11
      - ccx21
      - ccx31
      - ccx41
      - ccx51
    default: cx11
  image:
    doc:
      short_help: The OS image of the server.
    type: string
    required: false
    default: ubuntu-18.04
  wait_for_ssh:
    doc:
      short_help: Whether to wait for ssh to become available
    type: boolean
    required: false
    default: true
  extra_wait_time:
    doc:
      short_help: Extra wait time after ssh is ready.
    type: integer
    required: false
    default: 0
  ssh_keys:
    doc:
      short_help: List of SSH key names that correspond to the SSH keys configured for your Hetzner Cloud account access.
    type: list
    schema:
      type: string
    required: false
    empty: true
    cli:
      param_decls:
        - "--ssh-key"
frecklets:
  - frecklet:
      name: hcloud_server
      type: ansible-module
      register:
        target: "{{:: register_target ::}}"
        value: "{{ __result__.hcloud_server }}"
      properties:
        elevated: false
        internet: true
        idempotent: true
      resources:
        python-package:
          - hcloud
    task:
      become: false
    vars:
      api_token: "{{:: api_token ::}}"
      name: "{{:: server_name ::}}"
      server_type: "{{:: server_type ::}}"
      image: "{{:: image ::}}"
      ssh_keys: "{{:: ssh_keys ::}}"
      state: present
  - wait-for-ssh:
      frecklet::skip: "{{:: wait_for_ssh | negate ::}}"
      host: "{{ {{:: register_target ::}}.hcloud_server.ipv4_address }}"
      extra_wait_time: "{{:: extra_wait_time ::}}"
